package BP.WF.Template;

import BP.DA.*;
import BP.En.*;
import BP.Port.*;
import BP.Sys.*;
import BP.WF.*;
import java.util.*;

/** 
 属性
*/
public class SysFormTreeAttr extends EntityTreeAttr
{
	/** 
	 数据源
	*/
	public static final String DBSrc = "DBSrc";
	/** 
	 是否是目录
	*/
	public static final String IsDir = "IsDir";
	/**
	 组织编号
	 */
	public static final String OrgNo = "OrgNo";
}